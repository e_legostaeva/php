<html>
<head>
    <title> Task 5 </title>
</head>
<body>

<?php
function print_foreach($array)
{
    foreach ($array as $ind) {
        print "$ind" . str_repeat('&nbsp;', 2);
    };
}

// ----------1----------
$treug = array();
for ($n = 1; $n <= 10; $n++) {
    $treug[$n - 1] = $n * ($n + 1) / 2;
}

print_foreach($treug);
print '<br/>';

// ----------2----------
$kvd = array();
for ($n = 1; $n <= 10; $n++) {
    $kvd[$n - 1] = $n * $n;
}

print_foreach($kvd);
print '<br/>';

// ----------3-----------
$rez = array_merge($treug, $kvd);
print_foreach($rez);
print '<br/>';

// ---------4-----------
sort($rez);
print_foreach($rez);
print '<br/>';

// ---------5-----------
array_shift($rez);
print_foreach($rez);
print '<br/>';

// ---------6-----------
$rez = array_unique($rez);
print_foreach($rez);
?>
</body>
</html>