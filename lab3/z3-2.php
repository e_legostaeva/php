<html>
<head>
    <title> Task 2 </title>
</head>
<body>

<?php

$blueColor = "style='color : blue';";
$redColor = "style='color : red';";

print "<table border=1 cellpadding=5>\n";
for ($y = 0; $y <= 10; $y++) {
    print "<tr>\n";
    for ($x = 0; $x <= 10; $x++) {
        if ($x == 0 && $y == 0) {
            print "<td " . $redColor . ">+</td>\n";
        } elseif ($x == 0 || $y == 0) {
            print "<td " . $blueColor . ">" . ($x + $y) . "</td>\n";
        } else {
            print "\t<td>" . ($x + $y) . "</td>\n";
        }
    }
    print "</tr>\n";
}
print "</table>";

?>
</body>
</html>