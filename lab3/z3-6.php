<html>
<head>
    <title> Task 6 </title>
</head>
<body>

<?php
function print_foreach($array)
{
    foreach ($array as $key => $value) {
        print "$key => $value<br/>";
    }
}

// ----------1----------
$cust = array(
    'cnum' => 2001,
    'cname' => 'Hoffman',
    'city' => 'London',
    'snum' => 1001,
    'rating' => 100
);
print_foreach($cust);
print "-------------------------<br/>";

// ----------2----------
asort($cust);
print_foreach($cust);
print "-------------------------<br/>";

// ----------3----------
ksort($cust);
print_foreach($cust);
print "-------------------------<br/>";

// ----------4----------
sort($cust);
print_foreach($cust);
?>
</body>
</html>